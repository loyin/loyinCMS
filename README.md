#loyinCMS
##使用的是postgresql数据库

需要将doc/database/cms_20160117.backup导入到数据库cms中

数据库配置是在src/main/resource/config.txt中。

然后直接部署到tomcat中即可。

部署方式如图：

![eclipse中tomcat部署](http://git.oschina.net/uploads/images/2016/0117/210811_2cc21228_5147.jpeg "eclipse中tomcat部署")

**add External Web Module 方式来部署**

由于采用ROOT contentPath 所以需要讲path设置为 / 。这样做的好处时，在IDEA 下可以只能提示js和css代码。

还有一个原因是我觉得项目是发布在ROOT下 即也可以认为是一个域名下，避免相对路径的混淆。

后台登陆地址 http://127.0.0.1:8080/admin 

用户名admin 密码123456 密码加密采用DES加密

数据库设计文件为doc/databse/cms.erm 采用eclipse的ERmaster来设计。

![登陆界面](http://git.oschina.net/uploads/images/2016/0117/181846_21a63e9c_5147.jpeg "登陆界面")

![菜单](http://git.oschina.net/uploads/images/2016/0117/181913_ffb4ed75_5147.jpeg "菜单")

![角色编辑页面](http://git.oschina.net/uploads/images/2016/0117/181937_c590eddd_5147.jpeg "角色编辑页面")

#后续会继续完善，因为此版本是给公司的下属学校做内容管理用，因为时间仓促，目前暂时一些简单的功能。后续会继续完善。

#敬请期待

**数据库设计 采用ERmaster(eclipse 插件)设计**

![数据库ERmaster设计截图](http://git.oschina.net/uploads/images/2016/0117/213147_2db8b05a_5147.png "数据库ERmaster设计截图")

