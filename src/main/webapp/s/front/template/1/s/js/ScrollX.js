// JavaScript Document
(function($){
	$.number = function (num) {
        return parseInt(num.replace("px", ""));
    }
	$.fn.ScrollX = function(o){
		var o =  $.extend({speed:"slow",num:4,easing:"easeOutQuad"}, o || {});
		this.each(function(){
		    var $tab = $(this).find(".scrollList");
			var $tabConten = $(this).find(".div_conten ul");
			var $tabContenList = $tabConten.find("li");
			var $this = $tab.find("ul");
			var $list = $tab.find("li");
			var $prev = $(this).find(".prev");
			var $next = $(this).find(".next");
			if($list.length<=o.num){
				$prev.hide();
				$next.hide();
				return;	
			}
			var listWidth = $list.width()+$.number($list.css("marginRight"))+$.number($list.css("marginLeft"))+$.number($list.css("paddingRight"))+$.number($list.css("paddingLeft"));
			var listWidth2 = $tabContenList.width();
			var ScrollWidth = listWidth*o.num;
			var ScrollWidth2 = listWidth2*o.num;
			var Width = $list.length*listWidth;
			var Width2 = $list.length*listWidth2;
			var Left = 0;
			var Left2 = 0;
			$this.css("width",Width);
			$tabConten.css("width",Width2);
			$prev.addClass("no");
			$prev.click(function(){
				Left = Left+ScrollWidth;
				Left2 = Left2+ScrollWidth2;
				scrollPlay();
				$next.removeClass("no");
			})
			$next.click(function(){
				Left = Left-ScrollWidth;
				Left2 = Left2-ScrollWidth2;
				scrollPlay();
				$prev.removeClass("no");	
			})
			function scrollPlay(){
				if(Left>0){
					Left = 0;
					Left2 = 0;
					$prev.addClass("no");
					
				}else if(Left<-Width+ScrollWidth){
					Left = -Width+ScrollWidth;
					Left2 = -Width2+ScrollWidth2;	
					$next.addClass("no");
					
				}
				$this.animate({left:Left},o.speed,o.easing);
				$tabConten.animate({left:Left2},o.speed,o.easing);
			}
		})
	}
})(jQuery)