package net.loyin.app;

/**
 * Created by 龙影 on 2015-01-24.
 */
public class Constant {
    /**数据库类型*/
    public static final String DB_TYPE="db_type";
    /**数据库配置是否加密*/
    public static final String DB_ENCRYPT="db_encrypt";
    /**数据库配置*/
    public static final String DB_DRIVER = "db_driver";
    public static final String DB_USERNAME = "db_username";
    public static final String DB_PASSWORD = "db_password";
    public static final String DB_URL = "db_url";
    public static final String DB_DATABASE = "db_database";
    public static final String PAGE_SIZE = "page.size";
    public static final int PAGE_SIZE_DEF =20;
    /**des加密key*/
    public static final String SAFE_DES_KEY = "safe_des_key";
    /**jfinal 开发模式*/
    public static final String JFINAL_DEVMODE = "jfinal.devmode";
    public static final boolean JFINAL_DEVMODE_DEF =false;
    /**文件上传最大限制*/
    public static final String FILE_UPLOAD_MAXSIZE = "file_upload_maxsize";
    /**文件上传最大限制 默认值 100MB 单位kb*/
    public static final int FILE_UPLOAD_MAXSIZE_DEF =104857600;
    /**验证码令牌*/
    public static final String LOGIN_COOKIE_KEY="login_cookie_key";

    /**缓存key 1天*/
    public static final String CACHE_KEY_1DAY="1day";
    /**缓存key 1小时*/
    public static final String CACHE_KEY_1HOUR="1hour";
    /**缓存key 1分钟*/
    public static final String CACHE_KEY_1MIN="1min";
    public static final String CACHE_KEY_30MIN ="30mins";
    public static final String CACHE_KEY_10MIN ="10mins";
    public static final String SYSTEM = "system.txt";
    public static final String WEB_FRONT_BEIAN = "web_front_beian";
}
