package net.loyin.app.ctrl.front;

import com.jfinal.aop.Before;
import com.jfinal.plugin.ehcache.CacheInterceptor;
import com.jfinal.plugin.ehcache.CacheName;
import net.loyin.ext.annotation.ControllerBind;
import net.loyin.app.model.Recruit;

/**
 * 在线招聘
 * Created by loyin on 16/1/12.
 */
@ControllerBind(route = RecruitCtrl.base)
public class RecruitCtrl extends FrontBaseCtrl{
    public static final String base="/recruit";
    /***
     * 招聘信息展示
     */
    @Before(CacheInterceptor.class)
    @CacheName("page")
    public void view(){
        this.setNavInfo(base);
        this.setAttr("po",Recruit.dao.findById(this.getPara(0)));
    }
    /***
     * 文章按类别
     * /page/{url}-{article_cate}
     */
    @Before(CacheInterceptor.class)
    @CacheName("page")
    public void index(){
        this.setNavInfo(base);
        this.keepPara();
        this.setAttr("list", Recruit.dao.findAll());
    }
}
