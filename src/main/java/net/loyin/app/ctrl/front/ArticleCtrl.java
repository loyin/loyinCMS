package net.loyin.app.ctrl.front;

import com.jfinal.aop.Before;
import com.jfinal.plugin.ehcache.CacheInterceptor;
import com.jfinal.plugin.ehcache.CacheName;
import net.loyin.ext.annotation.ControllerBind;
import net.loyin.app.model.Article;
import net.loyin.app.model.ArticleCate;
import org.apache.commons.lang.StringUtils;

/**
 * Created by loyin on 16/1/12.
 */
@ControllerBind(route =ArticleCtrl.base)
public class ArticleCtrl extends FrontBaseCtrl{
    public static final String base="/article";
    @Before(CacheInterceptor.class)
    @CacheName("page")
    public void view(){
        String id=this.getPara(0,"");
        this.setAttr("po", Article.dao.qryById(id));
    }
    /**设置文章阅读次数*/
    public void readCount(){
        String id=this.getPara(0,"");
        if(StringUtils.isNotBlank(id)){
            Article.dao.updateReadCount(id);
        }
        renderNull();
    }
    /***
     * 文章按类别
     * /page/{url}-{article_cate}
     */
    @Before(CacheInterceptor.class)
    @CacheName("page")
    public void list(){
        String article_cate=this.getPara(0,"");
        this.setAttr("p1",article_cate);
        this.setAttr("selfCate",ArticleCate.dao.findByCode(article_cate));
        this.keepPara();
        this.setAttr("page",Article.dao.pageQry(this.getParaToInt(1,1),this.getParaToInt(2,8),article_cate));
    }
    /***
     * 文章按类别
     * /page/{url}-{article_cate}
     */
    @Before(CacheInterceptor.class)
    @CacheName("page")
    public void page(){
        String article_cate=this.getPara(0,"");
        this.setNavInfo(base+"/page/"+article_cate);
        this.setAttr("p1",article_cate);
        this.keepPara();
        this.setAttr("page",Article.dao.pageQry(this.getParaToInt(1,1),this.getParaToInt(2,8),article_cate));
    }
    /***
     * 文章按类别
     * /page/{url}-{article_cate}
     */
    @Before(CacheInterceptor.class)
    @CacheName("page")
    public void imgPage(){
        String article_cate=this.getPara(0,"");
        this.setNavInfo(base+"/imgPage/"+article_cate);
        this.setAttr("p1",article_cate);
        this.keepPara();
        this.setAttr("page",Article.dao.pageQry(this.getParaToInt(1,1),this.getParaToInt(2,8),article_cate));
    }

    /**
     * /hview/{url}-{id}
     */
    @Before(CacheInterceptor.class)
    @CacheName("page")
    public void hview(){
        String id=this.getPara(0,"");
        this.setNavInfo(base+"/hview/"+id);
        if (StringUtils.isNotBlank(id)) {
            this.setAttr("po", Article.dao.qryById(id));
        }
    }
}
