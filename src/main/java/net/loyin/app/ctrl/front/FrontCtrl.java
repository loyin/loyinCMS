package net.loyin.app.ctrl.front;

import net.loyin.ext.annotation.ControllerBind;
import net.loyin.app.model.NavMenu;
import org.apache.commons.lang.StringUtils;

/**
 * 前端首页
 * Created by loyin on 16/1/12.
 */
@ControllerBind(route = "/")
public class FrontCtrl extends FrontBaseCtrl{
    /***
     * /{导航url}
     */
    public void index(){
        String para=this.getPara(0);
        if(StringUtils.isNotBlank(para)) {
            String jumpUrl = NavMenu.dao.hasJumpUrl("/"+ para, 1);
            if (StringUtils.isNotBlank(jumpUrl)) {
                this.forward(jumpUrl+(jumpUrl.indexOf("?")>-1?"&":"?")+"fromUrl=/"+ para);
            }
        }
    }
}
