package net.loyin.app.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseAlbumCate<M extends BaseAlbumCate<M>> extends Model<M> implements IBean {

	public void setId(String id) {
		set("id", id);
	}

	public String getId() {
		return get("id");
	}

	public void setName(String name) {
		set("name", name);
	}

	public String getName() {
		return get("name");
	}

	public void setEnName(String enName) {
		set("en_name", enName);
	}

	public String getEnName() {
		return get("en_name");
	}

	public void setSummary(String summary) {
		set("summary", summary);
	}

	public String getSummary() {
		return get("summary");
	}

	public void setSort(Integer sort) {
		set("sort", sort);
	}

	public Integer getSort() {
		return get("sort");
	}

	public void setParentid(String parentid) {
		set("parentid", parentid);
	}

	public String getParentid() {
		return get("parentid");
	}

	public void setLevelno(Integer levelno) {
		set("levelno", levelno);
	}

	public Integer getLevelno() {
		return get("levelno");
	}

	public void setStatus(Integer status) {
		set("status", status);
	}

	public Integer getStatus() {
		return get("status");
	}
	public void setCode(String code) {
		set("code", code);
	}

	public String getCode() {
		return get("code");
	}

}
