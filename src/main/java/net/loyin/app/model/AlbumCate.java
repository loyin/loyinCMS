package net.loyin.app.model;

import net.loyin.ext.annotation.TableBind;
import net.loyin.app.model.base.BaseAlbumCate;

/**
 * 相册类别
 */
@SuppressWarnings("serial")
@TableBind(tableName = AlbumCate.tableName)
public class AlbumCate extends BaseAlbumCate<AlbumCate> {
	public static final String tableName="album_cate";
	public static final AlbumCate dao = new AlbumCate();
}
