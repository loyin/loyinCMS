package net.loyin.app.model;

import net.loyin.ext.annotation.TableBind;
import net.loyin.app.model.base.BaseArticleCate;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
@TableBind(tableName = ArticleCate.tableName)
public class ArticleCate extends BaseArticleCate<ArticleCate> {
	public static final String tableName="article_cate";
	public static final ArticleCate dao = new ArticleCate();
	public ArticleCate findByCode(String code){
		return this.findFirst("select a.*,p.name pname,p.code pcode from article_cate as a left join article_cate as p on p.id=a.parentid where a.code=? ",code);
	}
}
