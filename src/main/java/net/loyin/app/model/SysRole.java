package net.loyin.app.model;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import net.loyin.ext.annotation.TableBind;
import net.loyin.app.model.base.BaseSysRole;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色
 */
@SuppressWarnings("serial")
@TableBind(tableName = SysRole.tableName)
public class SysRole extends BaseSysRole<SysRole> {
	public static final String tableName="sys_role";
	public static final SysRole dao = new SysRole();

	/**
	 * 配置菜单权限
	 * @param role_id
	 * @param menuList
     */
	public void cfgMenu(String role_id, String[] menuList) {
		Db.update("delete from sys_role_menu where role_id=?",role_id);
		if(menuList!=null&&menuList.length>0) {
			String[][]paras=new String[menuList.length][2];
			for(int i=0;i<menuList.length;i++){
				paras[i][0]=role_id;
				paras[i][1]=menuList[i];
			}
			Db.batch("insert into sys_role_menu (role_id,menu_id) values(?,?)", paras,10);
		}
	}
	/***
	 * 获取角色菜单id
	 * @param role_id
	 * @return
	 */
	public List<String> qryRoleMenuId(String role_id){
		List<Record>list=Db.find("select menu_id from sys_role_menu where role_id=?",role_id);
		List<String> roles=new ArrayList<String>();
		if(list!=null&&list.isEmpty()==false){
			for(Record r : list){
				roles.add(r.getStr("menu_id"));
			}
		}
		return roles;
	}
}
